<?php

namespace CP\CreapBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Doctrine\ORM\EntityManagerInterface;
use CP\CreapBundle\Entity\MenuNode;

class MenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $em;
    private $factory;
    private $menu;

    public function __construct(FactoryInterface $factory, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->factory = $factory;
    }

    public function getMenu(array $options)
    {
        $this->menu = $this->factory->createItem($options['menu']);

        $menuNodes = $this->em->getRepository(MenuNode::class)->getTree($options['menu']);
        foreach ($menuNodes as $menuNode) {
            $this->getNode($menuNode);
        }

        return $this->menu;
    }

    private function getNode($node, $parent = null)
    {
        if ($node->getEnabled()) {
            $routeType = ('System router' === $node->getRouteEntity()) ? 'route' : 'uri';
            $menuParameters = [
                $routeType => $node->getRoute(),
                'routeParameters' => $node->getRouteParameters(),
                'attributes' => [
                    'options' => $node->getAttributes(),
                    'title' => $node->getTitleAttributes(),
                    'link' => $node->getLinkAttributes(),
                    'child' => $node->getChildAttributes(),
                    'extra' => $node->getExtraAttributes(),
                    'image' => $node->getImage(),
                ],
            ];
            (null !== $parent) ? $parent->addChild($node->getTitle(), $menuParameters) : $this->menu->addChild($node->getTitle(), $menuParameters);
            if (null !== $node->getChildren()) {
                $parent = (null === $parent) ? $this->menu[$node->getTitle()] : $parent[$node->getTitle()];
                foreach ($node->getChildren() as $childNode) {
                    $this->getNode($childNode, $parent);
                }
            }
        }
    }
}