<?php

namespace CP\CreapBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Menu
 *
 * @ORM\Table(name="menu")
 * @ORM\Entity(repositoryClass="CP\CreapBundle\Repository\MenuRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title", "id"})
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="MenuNode", mappedBy="menu")
     */
    private $nodes;


    /**
     * Menu constructor
     */
    public function __construct()
    {
        $this->slides = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param bool $enabled
     *
     * @return Menu
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Menu
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Menu
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set nodes
     *
     * @param MenuNode $nodes
     *
     * @return Menu
     */
    public function setNodes(MenuNode $nodes = null)
    {
        $this->nodes = $nodes;

        return $this;
    }

    /**
     * Get nodes
     *
     * @return MenuNode[]|Collection
     */
    public function getNodes()
    {
        return $this->nodes;
    }

    /**
     * Add nodes
     *
     * @param MenuNode $nodes
     * @return Menu
     */
    public function addNodes(MenuNode $nodes)
    {
        $nodes->setMenu($this);
        $this->nodes->add($nodes);

        return $this;
    }

    /**
     * Remove nodes
     *
     * @param Menu $nodes
     */
    public function removeNodes(MenuNode $nodes)
    {
        $nodes->setMenu();
        $this->nodes->removeElement($nodes);
    }
}

