<?php

namespace CP\CreapBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use CP\CreapBundle\Tree\RecursiveIterator;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Entity\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * MenuNode
 *
 * @ORM\Table(name="menu_node")
 * @ORM\Entity(repositoryClass="CP\CreapBundle\Repository\MenuNodeRepository")
 * @Gedmo\Tree(type="nested")
 * @Vich\Uploadable
 */
class MenuNode implements \IteratorAggregate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="nodes")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $menu;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255, nullable=true)
     */
    private $route;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer")
     */
    private $priority = 1;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @ORM\ManyToOne(targetEntity="RouteNode", inversedBy="menuNodes")
     * @ORM\JoinColumn(name="route_node_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $routeNode;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="MenuNode")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="MenuNode", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="MenuNode", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * @var array
     *
     * @ORM\Column(name="route_parameters", type="json_array", nullable=true)
     */
    private $routeParameters;

    /**
     * @var array
     *
     * @ORM\Column(name="attributes", type="json_array", nullable=true)
     */
    private $attributes;

    /**
     * @var array
     *
     * @ORM\Column(name="title_attributes", type="json_array", nullable=true)
     */
    private $titleAttributes;

    /**
     * @var array
     *
     * @ORM\Column(name="link_attributes", type="json_array", nullable=true)
     */
    private $linkAttributes;

    /**
     * @var array
     *
     * @ORM\Column(name="child_attributes", type="json_array", nullable=true)
     */
    private $childAttributes;

    /**
     * @var array
     *
     * @ORM\Column(name="extra_attributes", type="json_array", nullable=true)
     */
    private $extraAttributes;

    /**
     * @Vich\UploadableField(mapping="upload", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $image;

    /**
     * @var null
     */
    private $routeNodeSelect = null;


    /**
     * MenuNode constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add menu
     *
     * @param Menu $menu
     *
     * @return MenuNode
     */
    public function addMenu(Menu $menu)
    {
        $this->menu[] = $menu;

        return $this;
    }

    /**
     * Remove menu
     *
     * @param Menu $menu
     */
    public function removeMenu(Menu $menu)
    {
        $this->menu->removeElement($menu);
    }

    /**
     * Get menu
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Set menu
     *
     * @param Menu $menu
     *
     * @return MenuNode
     */
    public function setMenu(Menu $menu = null)
    {
        $this->menu = $menu;

        return $this;
    }








    /**
     * Set title
     *
     * @param string $title
     *
     * @return MenuNode
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set route
     *
     * @param string $route
     *
     * @return MenuNode
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string
     */
    public function getRoute()
    {
        if (null !== $this->route && null === $this->routeNode) {

            return $this->route;
        }

        if (null === $this->route && null !== $this->routeNode) {

            return $this->routeNode->getRoute();
        }
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return MenuNode
     */
    public function setPriority($priority = 1)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set enabled
     *
     * @param bool $enabled
     *
     * @return News
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add routeNode
     *
     * @param RouteNode $routeNode
     *
     * @return MenuNode
     */
    public function addRouteNode(RouteNode $routeNode)
    {
        $this->routeNode[] = $routeNode;

        return $this;
    }

    /**
     * Remove routeNode
     *
     * @param RouteNode $routeNode
     */
    public function removeRouteNode(RouteNode $routeNode)
    {
        $this->routeNode->removeElement($routeNode);
    }

    /**
     * Get routeNode
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRouteNode()
    {
        return $this->routeNode;
    }

    /**
     * Set routeNode
     *
     * @param RouteNode $routeNode
     *
     * @return MenuNode
     */
    public function setRouteNode(RouteNode $routeNode = null)
    {
        $this->routeNode = $routeNode;

        return $this;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setParent(MenuNode $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set routeParameters
     *
     * @param array $routeParameters
     *
     * @return MenuNode
     */
    public function setRouteParameters($routeParameters)
    {
        $this->routeParameters = $routeParameters;

        return $this;
    }

    /**
     * Get routeParameters
     *
     * @return array
     */
    public function getRouteParameters()
    {
        return $this->routeParameters;
    }

    /**
     * Set attributes
     *
     * @param array $attributes
     *
     * @return MenuNode
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Get attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set titleAttributes
     *
     * @param array $titleAttributes
     *
     * @return MenuNode
     */
    public function setTitleAttributes($titleAttributes)
    {
        $this->titleAttributes = $titleAttributes;

        return $this;
    }

    /**
     * Get titleAttributes
     *
     * @return array
     */
    public function getTitleAttributes()
    {
        return $this->titleAttributes;
    }

    /**
     * Set linkAttributes
     *
     * @param array $linkAttributes
     *
     * @return MenuNode
     */
    public function setLinkAttributes($linkAttributes)
    {
        $this->linkAttributes = $linkAttributes;

        return $this;
    }

    /**
     * Get linkAttributes
     *
     * @return array
     */
    public function getLinkAttributes()
    {
        return $this->linkAttributes;
    }

    /**
     * Set childAttributes
     *
     * @param array $childAttributes
     *
     * @return MenuNode
     */
    public function setChildAttributes($childAttributes)
    {
        $this->childAttributes = $childAttributes;

        return $this;
    }

    /**
     * Get childAttributes
     *
     * @return array
     */
    public function getChildAttributes()
    {
        return $this->childAttributes;
    }

    /**
     * Set extraAttributes
     *
     * @param array $extraAttributes
     *
     * @return MenuNode
     */
    public function setExtraAttributes($extraAttributes)
    {
        $this->extraAttributes = $extraAttributes;

        return $this;
    }

    /**
     * Get extraAttributes
     *
     * @return array
     */
    public function getExtraAttributes()
    {
        return $this->extraAttributes;
    }

    /**
     * @return RecursiveIterator
     */
    public function getIterator()
    {
        return new RecursiveIterator($this->getChildren());
    }

    /**
     * Set imageFile
     *
     * @param File $imageFile
     *
     * @return MenuNode
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $image
     *
     * @return MenuNode
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param $routeNodeSelect
     * @return $this
     */
    public function setRouteNodeSelect($routeNodeSelect)
    {
        $this->routeNodeSelect = $routeNodeSelect;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getRouteNodeSelect()
    {
        if (null !== $this->routeNodeSelect) {

            return $this->routeNodeSelect;
        }

        if (null !== $this->routeNode && null === $this->route) {

            return $this->routeNode->getId();
        }

        if (null !== $this->route && null === $this->routeNode) {

            return $this->route;
        }
    }

    /**
     * @return string
     */
    public function getRouteEntity()
    {
        if (null !== $this->routeNode && null === $this->route) {

            return $this->routeNode->getEntity();
        }

        if (null !== $this->route && null === $this->routeNode) {

            return 'System router';
        }
    }
}