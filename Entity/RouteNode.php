<?php

namespace CP\CreapBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * RouteNode
 *
 * @ORM\Table(name="route_node")
 * @ORM\Entity(repositoryClass="CP\CreapBundle\Repository\RouteNodeRepository")
 */
class RouteNode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255, nullable=true)
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=255)
     */
    private $entity;

    /**
     * @var int
     *
     * @ORM\Column(name="entity_node_id", type="integer")
     */
    private $entityNodeId;

    /**
     * @ORM\OneToMany(targetEntity="MenuNode", mappedBy="routeNode")
     */
    private $menuNodes;

    /**
     * RouteNode constructor
     */
    public function __construct()
    {
        $this->menuNodes = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return RouteNode
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set route
     *
     * @param string $route
     *
     * @return RouteNode
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set entity
     *
     * @param string $entity
     *
     * @return RouteNode
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set entityNodeId
     *
     * @param int $entityNodeId
     *
     * @return RouteNode
     */
    public function setEntityNodeId($entityNodeId)
    {
        $this->entityNodeId = $entityNodeId;

        return $this;
    }

    /**
     * Get entityNodeId
     *
     * @return string
     */
    public function getEntityNodeId()
    {
        return $this->entityNodeId;
    }

    /**
     * Set menuNodes
     *
     * @param MenuNode $menuNodes
     *
     * @return RouteNode
     */
    public function setMenuNodes(MenuNode $menuNodes = null)
    {
        $this->menuNodes = $menuNodes;

        return $this;
    }

    /**
     * Get menuNodes
     *
     * @return Engine[]|Collection
     */
    public function getMenuNodes()
    {
        return $this->menuNodes;
    }

    /**
     * Add menuNode
     *
     * @param MenuNode $menuNode
     *
     * @return RouteNode
     */
    public function addEngine(MenuNode $menuNode)
    {
        $menuNode->setModel($this);
        $this->menuNodes->add($menuNode);

        return $this;
    }

    /**
     * Remove menuNode
     *
     * @param MenuNode $menuNode
     */
    public function removeEngine(MenuNode $menuNode)
    {
        $menuNode->setModel();
        $this->menuNodes->removeElement($menuNode);
    }
}

