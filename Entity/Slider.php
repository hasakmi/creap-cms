<?php

namespace CP\CreapBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Slider
 *
 * @ORM\Table(name="slider")
 * @ORM\Entity(repositoryClass="CP\CreapBundle\Repository\SliderRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Slider
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title", "id"})
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="Slides", mappedBy="slider")
     */
    private $slides;


    /**
     * Slider constructor
     */
    public function __construct()
    {
        $this->slides = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param bool $enabled
     *
     * @return Slider
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Slider
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Slider
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slides
     *
     * @param Slides $slides
     *
     * @return Slider
     */
    public function setSlides(Slides $slides = null)
    {
        $this->slides = $slides;

        return $this;
    }

    /**
     * Get slides
     *
     * @return Slides[]|Collection
     */
    public function getSlides()
    {
        return $this->slides;
    }

    /**
     * Add slides
     *
     * @param Slides $slides
     *
     * @return Slider
     */
    public function addSlides(Slides $slides)
    {
        $slides->setSlider($this);
        $this->slides->add($slides);

        return $this;
    }

    /**
     * Remove slides
     *
     * @param Slides $slides
     */
    public function removeSlides(Slides $slides)
    {
        $slides->setSlider();
        $this->slides->removeElement($slides);
    }
}

