<?php

namespace CP\CreapBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    const ROLES = [
        'ROLE_DEFAULT' => 'user.role.user',
        'ROLE_ADMIN'   => 'user.role.admin',
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    protected $role;

    /**
     * @ORM\OneToMany(targetEntity="News", mappedBy="author")
     */
    private $news;


    /**
     * User constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->author = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        $highestRole = $this->roles[0];
        //return constant('self::' . implode(', ', $this->roles));
        return self::ROLES[$highestRole];
    }
}