<?php

namespace CP\CreapBundle\EventSubscriber;

use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;

class EasyAdminSaveAndStayButtonEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            EasyAdminEvents::POST_UPDATE => 'updateRequestReferer',
            EasyAdminEvents::POST_PERSIST => 'updateRequestReferer',
        ];
    }

    public function updateRequestReferer(GenericEvent $event)
    {
        /* @var $request Request */
        $request = $event->getArgument('request');

        if (!$request->request->has('save_and_stay')) {
            return;
        }

        $uri = $request->getRequestUri();
        parse_str(rawurldecode($uri), $output);
        unset($output['referer']);
        $uri = '';
        $i = 1;
        foreach ($output as $key => $value) {
            if ($i > 1) {
                $uri .= '&';
            }
            $uri .= $key.'='.$value;
            $i++;
        }
        $uri .= '&referer='.urlencode(urlencode(urlencode(str_replace('action=edit', 'action=list', str_replace('action=new', 'action=edit', $uri)))));
        $uri = str_replace('action=new', 'action=edit&id='.$event->getArgument('entity')->getId(), $uri);

        $request->query->set('referer', $uri);
    }
}
