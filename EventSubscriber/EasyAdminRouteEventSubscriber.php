<?php

namespace CP\CreapBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use CP\CreapBundle\Entity\RouteNode;

class EasyAdminRouteEventSubscriber implements EventSubscriberInterface
{

    private $isInstance = false;
    private $entities;
    private $entity;
    private $subject;
    private $em;
    private $fullPath;

    /**
     * EasyAdminRouteEventSubscriber constructor.
     * @param $entities
     */
    public function __construct($entities)
    {
        $this->entities = $entities;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            'easy_admin.post_persist' => array('setRouteNode'),
            'easy_admin.post_update' => array('updateRouteNode'),
            'easy_admin.pre_remove' => array('removeRouteNode'),
        );
    }

    /**
     * @param GenericEvent $event
     */
    public function setRouteNode(GenericEvent $event)
    {
        if (!$this->routeHandler($event)) {
            return;
        }
        $routeNode = new RouteNode();
        $routeNode
            ->setTitle($this->subject->getTitle())
            ->setRoute('/'.$this->fullPath)
            ->setEntity($this->entity)
            ->setEntityNodeId($this->subject->getId())
        ;
        $this->em->persist($routeNode);
        $this->em->flush();
    }

    /**
     * @param GenericEvent $event
     */
    public function updateRouteNode(GenericEvent $event)
    {
        if (!$this->routeHandler($event)) {
            return;
        }
        $routeNode = $this->em->getRepository(RouteNode::class)->findOneBy(
            [
                'entity' => $this->entity,
                'entityNodeId' => $this->subject->getId()
            ]
        );

        $persist = false;
        if (!$routeNode) {
            $persist = true;
            $routeNode = new RouteNode();
            $routeNode->setEntityNodeId($this->subject->getId());
        }
        $routeNode
            ->setTitle($this->subject->getTitle())
            ->setRoute('/'.$this->fullPath)
            ->setEntity($this->entity)
        ;
        if ($persist) {
            $this->em->persist($routeNode);
        }
        $this->em->flush();

        if (method_exists($this->em->getRepository($this->entity),'getChildren')) {
            foreach ($this->em->getRepository($this->entity)->getChildren($this->subject) as $child) {

                $nodeArray = [];
                if (method_exists($this->em->getRepository($this->entity), 'getPath')) {
                    foreach ($this->em->getRepository($this->entity)->getPath($child) as $node) {
                        $nodeArray[] = $node->getSlug();
                    }
                    $fullPath = implode('/', $nodeArray);
                } else {
                    $fullPath = $child->getSlug();
                }

                $routeNodeChild = $this->em->getRepository(RouteNode::class)->findOneBy(
                    [
                        'entity' => $this->entity,
                        'entityNodeId' => $child->getId()
                    ]
                );

                $persist = false;
                if (!$routeNodeChild) {
                    $persist = true;
                    $routeNodeChild = new RouteNode();
                    $routeNodeChild->setEntityNodeId($child->getId());
                }
                $routeNodeChild
                    ->setTitle($child->getTitle())
                    ->setRoute('/'.$fullPath)
                    ->setEntity($this->entity);
                if ($persist) {
                    $this->em->persist($routeNodeChild);
                }
                $this->em->flush();
            }
        }
    }

    /**
     * @param GenericEvent $event
     */
    public function removeRouteNode(GenericEvent $event)
    {
        if (!$this->routeHandler($event)) {
            return;
        }
        $routeNode = $this->em->getRepository(RouteNode::class)->findOneBy(
            [
                'entity' => $this->entity,
                'entityNodeId' => $this->subject->getId()
            ]
        );

        if (method_exists($this->em->getRepository($this->entity),'getChildren')) {
            foreach ($this->em->getRepository($this->entity)->getChildren($this->subject) as $child) {
                $routeNodeChild = $this->em->getRepository(RouteNode::class)->findOneBy(
                    [
                        'entity' => $this->entity,
                        'entityNodeId' => $child->getId()
                    ]
                );
                $this->em->remove($routeNodeChild);
            }
        }

        $this->em->remove($routeNode);
        $this->em->flush();
    }

    /**
     * @param $event
     */
    private function routeHandler($event)
    {
        if ($this->isInstance($event)) {
            if (method_exists($this->em->getRepository($this->entity), 'getPath')) {
                foreach ($this->em->getRepository($this->entity)->getPath($this->subject) as $node) {
                    $nodeArray[] = $node->getSlug();
                }
                $this->fullPath = implode('/', $nodeArray);
            } elseif (method_exists($this->subject, 'getSlug')) {
                $this->fullPath = $this->subject->getSlug();
            } else {
                $this->isInstance = false;
            }
        }

        return ($this->isInstance) ? true : false;
    }

    /**
     * @param $event
     * @return bool
     */
    private function isInstance($event)
    {
        $this->subject = $event->getSubject();
        $this->em = $event->getArgument('em');

        foreach ($this->entities as $e) {
            if ($this->subject instanceof $e) {
                $this->isInstance = true;
                $this->entity = $e;
                break;
            }
        }

        return ($this->isInstance) ? true : false;
    }
}