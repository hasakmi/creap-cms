<?php

namespace CP\CreapBundle\Repository;

use Doctrine\ORM\Query;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class MenuNodeRepository extends NestedTreeRepository
{
    public function getTree($menu)
    {
        return $this->createQueryBuilder('node')
            ->join('node.menu', 'menu')
            ->where('menu.slug = :menu')->setParameter('menu', $menu)
            ->orderBy('node.priority', 'ASC')
            ->getQuery()
            ->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
            ->getResult('tree')
            ;
    }
}

