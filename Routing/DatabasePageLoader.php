<?php

namespace CP\CreapBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class DatabasePageLoader extends Loader
{
    private $loaded = false;

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "page" loader twice');
        }

        $routes = new RouteCollection();

        // prepare a new route
        $path = '/{route}';
        $defaults = [
            '_controller' => 'CPCreapBundle:Routing/DatabasePage:dynamicPage',
        ];
        $requirements = [
            'route' => '.+',
        ];
        $route = new Route($path, $defaults, $requirements);

        // add the new route to the route collection
        $routeName = 'pageRoute';
        $routes->add($routeName, $route);

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'page' === $type;
    }
}