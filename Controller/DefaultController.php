<?php

namespace CP\CreapBundle\Controller;

use CP\CreapBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $homepage = $this->getDoctrine()
            ->getRepository(Page::class)
            ->findOneBy(['isHomepage' => true, 'enabled' => true]);

        if (!$homepage) {
            throw $this->createNotFoundException(
                'No homepage found!'
            );
        }

        return $this->render($homepage->getTemplate(), [
            'page' => $homepage,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
}
