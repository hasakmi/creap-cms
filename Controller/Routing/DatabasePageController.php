<?php

namespace CP\CreapBundle\Controller\Routing;

use CP\CreapBundle\Entity\Page;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DatabasePageController extends Controller
{
    public function dynamicPageAction($route)
    {
        $routeArray = explode('/', $route);

        $page = $this->getDoctrine()
            ->getRepository(Page::class)
            ->findOneBy(['slug' => end($routeArray), 'enabled' => true]);

        if (!$page) {
            throw $this->createNotFoundException(
                'No page found!'
            );
        }

        return $this->render($page->getTemplate(), [
            'page' => $page,
        ]);
    }
}