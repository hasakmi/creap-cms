<?php

namespace CP\CreapBundle\Controller\Admin;

use CP\CreapBundle\Entity\Slider;
use CP\CreapBundle\Entity\Slides;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SlidesController extends BaseAdminController
{
    /**
     * The method that is executed when the user performs a 'list' action on an entity.
     *
     * @return Response
     */
    protected function listAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        if (!$this->request->get('parent')) {
            return $this->redirectToRoute(
                'easyadmin',
                [
                    'action' => 'list',
                    'entity' => 'Slides',
                    'parent' => 1,
                ]
            );
        }

        $dql = $this->entity['list']['dql_filter'];
        $this->entity['list']['dql_filter'] = is_null($dql) ? 'entity.slider = '.$this->request->get('parent') : $dql.' AND entity.slider = '.$this->request->get('parent');

        $fields = $this->entity['list']['fields'];
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->config['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);
        $parents = $this->em->getRepository(Slider::class)->findAll(array(), array('title' => 'ASC'));
        $this->dispatch(EasyAdminEvents::POST_LIST, array('paginator' => $paginator));

        return $this->render($this->entity['templates']['list'], array(
            'parents' => $parents,
            'paginator' => $paginator,
            'fields' => $fields,
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ));
    }

    /**
     * The method that is executed when the user performs a 'sortItems' action on an entity.
     *
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function sortItemsAction(): RedirectResponse
    {
        /**
         * @var \CP\CreapBundle\Entity\Slides
         */
        $order = $this->request->query->get('order', '');
        if (!empty($order)) {

            $orders = array_flip(explode(',', $order));

            $faqs = $this->em->getRepository(Slides::class)->findBy([
                'slider' => $this->request->get('parent'),
            ]);

            if ($faqs) {
                foreach ($faqs as $faq) {
                    if (isset($orders[$faq->getId()])) {
                        $faq->setOrder($orders[$faq->getId()] + 1);
                        $this->em->persist($faq);
                    }
                }
                $this->em->flush();
            }
        }

        return $this->redirectToRoute('easyadmin', [
            'action' => 'list',
            'entity' => 'Slides',
            'parent' => $this->request->get('parent'),
        ]);
    }

    /**
     * @return Response
     */
    protected function backToListAction()
    {
        return $this->redirectToRoute('easyadmin', [
            'action' => 'list',
            'entity' => 'Slider',
        ]);
    }

    /**
     * Pre persist entity
     *
     * @param $entity
     */
    public function prePersistSlidesEntity($entity)
    {
        $slider = $this->request->get('parent');
        $repository = $this->getDoctrine()->getRepository(Slider::class);
        $slider = $repository->findOneById($slider);
        $entity->setSlider($slider);
    }
}