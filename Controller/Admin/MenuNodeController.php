<?php

namespace CP\CreapBundle\Controller\Admin;

use CP\CreapBundle\Entity\Menu;
use Doctrine\ORM\Query;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use CP\CreapBundle\Entity\RouteNode;
use Doctrine\ORM\EntityManagerInterface;

class MenuNodeController extends AdminController
{
    /**
     * The method that is executed when the user performs a 'list' action on an entity.
     *
     * @return Response
     */
    protected function listAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        if (!$this->request->get('parent')) {
            return $this->redirectToRoute(
                'easyadmin',
                [
                    'action' => 'list',
                    'entity' => 'MenuNode',
                    'parent' => 1,
                ]
            );
        }

        $dql = $this->entity['list']['dql_filter'];
        $this->entity['list']['dql_filter'] = is_null($dql) ? 'entity.menu = '.$this->request->get('parent') : $dql.' AND entity.menu = '.$this->request->get('parent');

        $fields = $this->entity['list']['fields'];
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->config['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);
        $parents = $this->em->getRepository(Menu::class)->findAll(array(), array('title' => 'ASC'));
        $this->dispatch(EasyAdminEvents::POST_LIST, array('paginator' => $paginator));

        return $this->render($this->entity['templates']['list'], array(
            'parents' => $parents,
            'paginator' => $paginator,
            'fields' => $fields,
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ));
    }

    protected function findAll($entityClass, $page = 1, $maxPerPage = 15, $sortField = null, $sortDirection = null, $dqlFilter = null)
    {
        $originalPaginator = parent::findAll($entityClass, $page, $maxPerPage, 'lft', 'ASC', $dqlFilter);

        $query = $originalPaginator->getAdapter()->getQuery();
        $result = $query->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getResult('tree');

        return new Pagerfanta(new ArrayAdapter($result));
    }

    /**
     * @return Response
     */
    protected function backToListAction()
    {
        return $this->redirectToRoute('easyadmin', [
            'action' => 'list',
            'entity' => 'Menu',
        ]);
    }

    /**
     * Pre persist entity
     *
     * @param $entity
     */
    public function prePersistMenuNodeEntity($entity)
    {
        $menu = $this->request->get('parent');
        $repository = $this->getDoctrine()->getRepository(Menu::class);
        $menu = $repository->findOneById($menu);
        $entity->setMenu($menu);

        if (is_numeric($entity->getRouteNodeSelect())) {
            $repository = $this->getDoctrine()->getRepository(RouteNode::class);
            $routeNode = $repository->findOneById($entity->getRouteNodeSelect());
            $entity->setRouteNode($routeNode);
            $entity->setRoute(null);
        } else {
            $entity->setRouteNode(null);
            $entity->setRoute($entity->getRouteNodeSelect());
        }
    }

    /**
     * Pre update entity
     *
     * @param $entity
     */
    public function preUpdateMenuNodeEntity($entity) {
        if (is_numeric($entity->getRouteNodeSelect())) {
            $repository = $this->getDoctrine()->getRepository(RouteNode::class);
            $routeNode = $repository->findOneById($entity->getRouteNodeSelect());
            $entity->setRouteNode($routeNode);
            $entity->setRoute(null);
        } else {
            $entity->setRouteNode(null);
            $entity->setRoute($entity->getRouteNodeSelect());
        }
    }
}
