<?php

namespace CP\CreapBundle\Controller\Admin;

use Doctrine\ORM\Query;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;

class PageController extends AdminController
{
    protected function findAll($entityClass, $page = 1, $maxPerPage = 15, $sortField = null, $sortDirection = null, $dqlFilter = null)
    {
        $originalPaginator = parent::findAll($entityClass, $page, $maxPerPage, 'lft', 'ASC', $dqlFilter);

        $query = $originalPaginator->getAdapter()->getQuery();
        $result = $query->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getResult('tree');

        return new Pagerfanta(new ArrayAdapter($result));
    }
}
