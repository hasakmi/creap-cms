<?php
/**
 * Created by PhpStorm.
 * User: behe
 * Date: 6.9.17
 * Time: 20:00
 */

namespace CP\CreapBundle\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class NewsController extends BaseAdminController
{

    public function prePersistNewsEntity($entity)
    {
        if (method_exists($entity, 'setAuthor')) {
            $entity->setAuthor($this->getUser());
        }
    }
}