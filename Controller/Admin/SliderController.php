<?php

namespace CP\CreapBundle\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;

class SliderController extends BaseAdminController
{
    /**
     * @return Response
     */
    protected function slidesAction()
    {
        $id = $this->request->get('id');

        return $this->redirectToRoute('easyadmin', [
            'action' => 'list',
            'entity' => 'Slides',
            'parent' => $id,
        ]);
    }
}