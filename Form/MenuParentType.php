<?php

namespace CP\CreapBundle\Form;

use CP\CreapBundle\Entity\MenuNode;
use CP\CreapBundle\Repository\MenuNodeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class MenuParentType extends AbstractType
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'class' => MenuNode::class,
                'query_builder' => function (MenuNodeRepository $er) {
                    return $er->createQueryBuilder('node')
                        ->join('node.menu', 'menu')
                        ->where('menu.id = :parent')->setParameter('parent', $this->requestStack->getCurrentRequest()->get('parent'))
                        ->orderBy('node.title', 'ASC');
                },
                'placeholder' => 'label.form.empty_value',
            ]
        );
    }

    public function getParent()
    {
        return EntityType::class;
    }
}