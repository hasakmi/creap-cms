<?php

namespace CP\CreapBundle\Form;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemplatesType extends ChoiceType
{
    private $loader;

    public function __construct(\Twig_Loader_Filesystem $loader, ChoiceListFactoryInterface $choiceListFactory = null)
    {
        parent::__construct($choiceListFactory);
        $this->loader = $loader;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $options = array_merge($options, $this->getChoicesAndAttrs($options['namespaces'], $options['paths']));
        parent::buildForm($builder, $options);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $options = array_merge($options, $this->getChoicesAndAttrs($options['namespaces'], $options['paths']));
        parent::buildView($view, $form, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'namespaces' => [],
            'paths' => [],
        ]);
    }

    private function getChoicesAndAttrs(array $namespaces = [], array $paths = [])
    {
        $choices = [];
        $attrs = [];

        foreach ($this->loader->getNamespaces() as $namespace) {
            if (!empty($namespaces) && !in_array($namespace, $namespaces, true)) {
                continue;
            }

            $dirs = array_filter($this->loader->getPaths($namespace), function ($path) use ($paths) {
                return empty($paths) || in_array($path, $paths, true);
            });

            $finder = Finder::create()->name('*.twig')->in($dirs)->sortByName();
            $namespace = $namespace === \Twig_Loader_Filesystem::MAIN_NAMESPACE ? '/' : '@'.$namespace;

            /* @var $file SplFileInfo */
            foreach ($finder as $file) {
                $name = rtrim($namespace, '/').'/'.$file->getRelativePathname();
                $dir = dirname($name);

                if (!isset($choices[$namespace][$dir])) {
                    $label = $this->makeLabel($dir);
                    $choices[$namespace][$label] = $dir;
                    $attrs[$label] = [
                        'disabled' => 'disabled',
                    ];
                }

                $choices[$namespace][$this->makeLabel($name)] = $name;
            }
        }

        return [
            'choices' => count($choices) === 1 ? reset($choices) : $choices,
            'choice_attr' => $attrs,
        ];
    }

    private function makeLabel($label)
    {
        return str_repeat('    ', substr_count($label, '/')).$label;
    }
}
