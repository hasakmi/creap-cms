<?php

namespace CP\CreapBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class SlideButtonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'slides.button.title'])
            ->add('route', RouteType::class, ['label' => 'slides.button.route'])
            ->add('url', UrlType::class, ['label' => 'slides.button.url'])
            ->add('css_class', TextType::class, ['label' => 'slides.button.css_class'])
            ->add('icon', TextType::class, ['label' => 'slides.button.icon'])
            ->add('identifier', TextType::class, ['label' => 'slides.button.identifier'])
        ;
    }
}