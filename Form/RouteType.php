<?php

namespace CP\CreapBundle\Form;

use Doctrine\Common\Util\ClassUtils;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\Form\ChoiceList\Factory\ChoiceListFactoryInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouterInterface;
use CP\CreapBundle\Entity\RouteNode;
use Doctrine\ORM\EntityManagerInterface;

class RouteType extends ChoiceType
{
    private $router;
    private $em;

    public function __construct(RouterInterface $router, EntityManagerInterface $em, ChoiceListFactoryInterface $choiceListFactory = null)
    {
        parent::__construct($choiceListFactory);
        $this->router = $router;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $options = array_merge($options, $this->getChoicesAndAttrs($options['entities']));
        parent::buildForm($builder, $options);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $options = array_merge($options, $this->getChoicesAndAttrs($options['entities']));
        parent::buildView($view, $form, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'entities' => [],
            'select2_config' => <<<'EOT'
                {
                    templateResult: function (state) {
                        var $el = $(state.element);
                        if (typeof $el.data('title')  !== "undefined") {
                            state.text = $el.data('title');
                        }
                        var $state = $('<span>' + state.text + '</span>');
                        if ($el.data('path') && $el.data('title') && $el.data('path') != $el.data('title')) {
                            $state.append('<span style="font-size: 75%;">     ' + $el.data('path') + '</span>');
                        }
                        
                        return $state;
                    },
                    templateSelection: function (state) {
                        var $el = $(state.element);
                        if (typeof $el.data('title')  !== "undefined") {
                            state.text = $el.data('title');
                        }
                        var $state = $('<span>' + state.text + '</span>');
                        if ($el.data('path') && $el.data('title') && $el.data('path') != $el.data('title')) {
                            $state.append('<span style="font-size: 75%;">     ' + $el.data('path') + '</span>');
                        }
                        
                        return $state;
                    } 
                }
EOT
        ]);
    }

    private function getChoicesAndAttrs($entities = [])
    {
        $choices = [];
        $attrs = [];

        $repository = $this->em->getRepository(RouteNode::class);
        $routeNodes = $repository->findBy(
            array(),
            array('entity' => 'ASC', 'title' => 'ASC')
        );

        foreach ($routeNodes as $node) {
            $choices[$node->getEntity()][$node->getRoute()] = $node->getId();
            $attrs[$node->getRoute()] = [
                'data-title' => $node->getTitle(),
                'data-path' => $node->getRoute(),
            ];
        }

        foreach ($this->router->getRouteCollection() as $name => $route) {
            $methods = $route->getMethods();
            if (!empty($methods) && !in_array('GET', $methods, true)) {
                continue;
            }

            $namespace = 'system.routes';
            if ($route->hasDefault(RouteObjectInterface::CONTENT_OBJECT)) {
                $class = ClassUtils::getClass($route->getDefault(RouteObjectInterface::CONTENT_OBJECT));
                if (false === $entities || (!empty($entities) && !is_array($class, $entities, true))) {
                    continue;
                }

                $namespace = $class;
            }

            $label = $this->makeLabel($route);
            $choices[$namespace][$label] = $name;
            $attrs[$label] = [
                'data-title' => $route->getPath(),
                'data-path' => $route->getPath(),
            ];
        }

        return [
            'choices' => count($choices) === 1 ? reset($choices) : $choices,
            'choice_attr' => $attrs,
        ];
    }

    private function makeLabel(Route $route)
    {
        $label = $route->getPath();
        if ($route->hasDefault(RouteObjectInterface::CONTENT_OBJECT)) {
            $object = $route->getDefault(RouteObjectInterface::CONTENT_OBJECT);
            if (method_exists($object, '__toString')) {
                $label = (string) $object;
            }
        }

        return $label;
    }
}
