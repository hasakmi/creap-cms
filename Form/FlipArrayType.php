<?php
/**
 * Created by PhpStorm.
 * User: behe
 * Date: 6.9.17
 * Time: 18:10
 */

namespace CP\CreapBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FlipArrayType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setNormalizer('choices', function($resolver, $data) {
            return array_flip($data);
        });
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

}