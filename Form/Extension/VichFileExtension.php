<?php
/**
 * Created by PhpStorm.
 * User: behe
 * Date: 14.9.17
 * Time: 20:04
 */

namespace CP\CreapBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Mapping\PropertyMappingFactory;

class VichFileExtension extends AbstractTypeExtension
{
    private $mappingFactory;

    public function __construct(PropertyMappingFactory $mappingFactory)
    {
        $this->mappingFactory = $mappingFactory;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
            $data = $event->getData();

            if (isset($data['file']) && is_string($data['file'])) {
                $form = $event->getForm()->getParent();

                $config = $event->getForm()->getConfig();
                $property = $config->getPropertyPath() ?? $config->getName();

                if ($form && ($object = $form->getData())) {
                    $mapping = $this->mappingFactory->fromField($object, $property);
                    $mapping->setFileName($object, $data['file']);
                }
            }
        });
    }

    public function getExtendedType()
    {
        return VichFileType::class;
    }
}