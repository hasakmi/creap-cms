<?php
/**
 * Created by PhpStorm.
 * User: behe
 * Date: 14.9.17
 * Time: 20:05
 */

namespace CP\CreapBundle\Form\Extension;

use Vich\UploaderBundle\Form\Type\VichImageType;

class VichImageExtension extends VichFileExtension
{
    public function getExtendedType(): string
    {
        return VichImageType::class;
    }
}
