<?php

namespace CP\CreapBundle\Form\Extension;

use CP\CreapBundle\Date\MomentFormatConverter;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateTimePickerExtension extends AbstractTypeExtension
{
    private $converter;

    public function __construct()
    {
        $this->converter = new MomentFormatConverter();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'widget' => 'single_text',
            'format' => \IntlDateFormatter::MEDIUM,
            'html5' => false,
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $dateFormat = is_int($options['date_format']) ? $options['date_format'] : DateTimeType::DEFAULT_DATE_FORMAT;
        $timeFormat = DateTimeType::DEFAULT_TIME_FORMAT;
        $calendar = \IntlDateFormatter::GREGORIAN;
        $pattern = is_string($options['format']) ? $options['format'] : null;

        $formatter = new \IntlDateFormatter(\Locale::getDefault(), $dateFormat, $timeFormat, $options['view_timezone'], $calendar, $pattern);
        $view->vars['format'] = $this->converter->convert($formatter->getPattern());
    }

    public function getExtendedType()
    {
        return DateTimeType::class;
    }
}
