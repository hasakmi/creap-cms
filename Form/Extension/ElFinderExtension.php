<?php
/**
 * Created by PhpStorm.
 * User: behe
 * Date: 14.9.17
 * Time: 20:07
 */

namespace CP\CreapBundle\Form\Extension;

use FM\ElfinderBundle\Form\Type\ElFinderType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ElFinderExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($elFinderType = $this->getElFinderType()) {
            $elFinderType->buildForm($builder, $options);
            $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                if (is_string($data)) {
                    $event->setData(ltrim($data, '/'));
                }
            });
        }
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($elFinderType = $this->getElFinderType()) {
            $elFinderType->buildView($view, $form, $options);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        if ($elFinderType = $this->getElFinderType()) {
            $elFinderType->configureOptions($resolver);
        }
    }

    public function getExtendedType()
    {
        return FileType::class;
    }

    private function getElFinderType()
    {
        if (class_exists(ElFinderType::class)) {
            return new ElFinderType();
        }

        return null;
    }
}