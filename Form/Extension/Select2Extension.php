<?php

namespace CP\CreapBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Select2Extension extends AbstractTypeExtension
{
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if (!in_array('choice', $view->vars['block_prefixes'], true)) {
            return;
        }

        if (!isset($view->vars['attr']['data-widget']) && empty($options['select2_config'])) {
            $view->vars['attr']['data-widget'] = 'select2';
        }

        $view->vars['select2_config'] = $options['select2_config'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'select2_config' => '',
        ]);
    }

    public function getExtendedType()
    {
        return FormType::class;
    }
}
