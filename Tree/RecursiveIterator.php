<?php

namespace CP\CreapBundle\Tree;

final class RecursiveIterator implements \RecursiveIterator, \OuterIterator
{
    private $iterator;

    public function __construct($iterator)
    {
        if (!$iterator instanceof \Traversable) {
            $iterator = new \RecursiveArrayIterator($iterator);
        } elseif ($iterator instanceof \IteratorAggregate) {
            $iterator = $iterator->getIterator();
        }

        $this->iterator = $iterator;
    }

    public function current()
    {
        return $this->iterator->current();
    }

    public function next()
    {
        $this->iterator->next();
    }

    public function key()
    {
        return $this->iterator->key();
    }

    public function valid()
    {
        return $this->iterator->valid();
    }

    public function rewind()
    {
        $this->iterator->rewind();
    }

    public function hasChildren()
    {
        return (count($this->iterator->current()->getChildren()) > 0) ? true : false;
    }

    public function getChildren()
    {
        return new self($this->iterator->current());
    }

    public function getInnerIterator()
    {
        return $this->iterator;
    }
}
