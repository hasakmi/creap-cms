CreapCMS
=========

Systém pro správu obsahu Creative Point.

#### Instalace


Do souboru ```composer.json``` je nutné přidat repozitář a závislost na ```Creap CMS Bundle```:

```
[...]
"require" : {
    [...]
    "cp/creapbundle" : "dev-master"
},
"repositories" : [{
    "type" : "vcs",
    "url" : "https://hasakmi@bitbucket.org/hasakmi/creap-cms.git"
}],
[...]
```

V souboru ```app/AppKernel.php``` zaregistrujete bundle pomocí:

```
new CP\CreapBundle\CreapBundle(),

//Custom Bundles
new FOS\UserBundle\FOSUserBundle(),
new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
new EasyCorp\Bundle\EasyAdminBundle\EasyAdminBundle(),
new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
new FM\ElfinderBundle\FMElfinderBundle(),
new Vich\UploaderBundle\VichUploaderBundle(),
new Burgov\Bundle\KeyValueFormBundle\BurgovKeyValueFormBundle(),
new Knp\Bundle\MenuBundle\KnpMenuBundle(),
new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
new JMS\I18nRoutingBundle\JMSI18nRoutingBundle(),
new JMS\TranslationBundle\JMSTranslationBundle(),
new Liip\ImagineBundle\LiipImagineBundle(),
new SunCat\MobileDetectBundle\MobileDetectBundle(),
```


#### Konfigurace

Zkopírujte nastavení rout do adresáře ```app/routing.yml```:

```
cp:
    resource: '@CPCreapBundle/Controller/'
    type: annotation

app:
    resource: '@AppBundle/Controller/'
    type: annotation

fos_user:
    resource: "@FOSUserBundle/Resources/config/routing/all.xml"
    
fos_js_routing:
    resource: "@FOSJsRoutingBundle/Resources/config/routing/routing.xml"

easy_admin_bundle:
    resource: "@EasyAdminBundle/Controller/"
    type:     annotation
    prefix:   /admin

elfinder:
    resource: "@FMElfinderBundle/Resources/config/routing.yml"
    
_liip_imagine:
    resource: "@LiipImagineBundle/Resources/config/routing.xml"
```

Přednastavená konfigurace pro ```app/security.yaml```:

```
security:
    encoders:
        FOS\UserBundle\Model\UserInterface: bcrypt

    role_hierarchy:
        ROLE_ADMIN: [ROLE_USER, ROLE_ALLOWED_TO_SWITCH]

    # https://symfony.com/doc/current/security.html#b-configuring-how-users-are-loaded
    providers:
        in_memory:
            memory: ~
        fos_userbundle:
            id: fos_user.user_provider.username

    firewalls:
        # disables authentication for assets and the profiler, adapt it according to your needs
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false

        main:
            pattern: ^/
            form_login:
                provider: fos_userbundle
                csrf_token_generator: security.csrf.token_manager

            logout:       true
            anonymous:    true


    access_control:
        - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/admin/, role: [ROLE_ADMIN] }
        - { path: ^/efconnect, role: ROLE_USER }
        - { path: ^/elfinder, role: ROLE_USER }
```

Nastavení je možné jakkoli změnit.


#### Assets
Veškeré ```public``` souboy jsou zkopírovány do adresář ```/web/bundles/cpcreap/```


#### EASY ADMIN konfigurace

**Menu:**
Chcete-li použít předdefinované menu, jednoduše importujte soubor ```/vendor/cp/creap-bundle/Resources/config/easy_admin/menu.yml``` do konfiguračního souboru Easy Admin:

```
imports:
    [...]
    - resource: '@CPCreapBundle/Resources/config/easy_admin/menu.yml'
```

Pokud je však potřeba vytvořit vlastní strukturu menu, zkopírujte obsah tohoto souboru a libovolně jej upravte.

**Entity**
Aby bylo možné entity upravovat dle potřeb webu, je nutné je importovat do konfiguračního souboru Easu Admin.

```
imports:
    [...]
    - resource: '@CPCreapBundle/Resources/config/easy_admin/entity/*.yml'
```
Výše uvedený příklad importuje všechny entity obsažené v CreapCMS. Chcete-li některou z entit upravit,
je nutné ji z importu odstranit a zbylé entity importovat jednotlivě:
```
imports:
    [...]
    - resource: '@CPCreapBundle/Resources/config/easy_admin/entity/user.yml'
```

#### Custom router a dynamycké stránky

**Dynamické stránky**
Chcete-li využívat možnost vytvářet stránky dynamicky v administraci, přidejte do ```app/routing.yml```
následující:

```
[...]

cp_creap_pages:
    resource: .
    type: page
```

Je třeba mét na paměti, že Symfony zpracovává routy postupně. Najde-li některý z dříve definovaných routerů
odpovídající routu, Loader ji načte a činnost routeru se přeruší. Při definovaní routeru je nutné mít tento
fakt na paměti.

**Dynamycké přiřazování šablon**
Pro zapnutí dynamického přiřazování šablon jednotlivým stránkám je nutné nastvait v konfiguraci twigu následující:

```
twig:
[...]
    paths:
        [...]
        '%kernel.project_dir%/app/Resources/views/dynamic_pages': ~
```

CreapCMS má v základní konfiguraci přednatavenou právě tuto cesu. Přejete-li si cestu změnit, je potřeba 
změnit i konfiguraci pole pro výběr adresáře s šablonami pro etnity ```Page```.


#### i18 Routing

Vytvořte konfiguraci bundlu:
```
jms_i18n_routing:
    default_locale: cs
    locales: [cs, en]
    strategy: prefix_except_default
```

Příkaz pro vygenerování lokalizovaných rout:

```
php bin/console translation:extract cs --enable-extractor=jms_i18n_routing --bundle=AppBundle --output-format=yml --exclude-dir=Tests
```

#### TODO
    - webpack asset: asi do adresáře Assets, pak se budou muset ručně přidat do webpack configu 
    - Časem přidat bundle pro revize textových stránek: EntityAuditBundle
