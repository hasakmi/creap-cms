<?php

namespace CP\CreapBundle\Twig;

use CP\CreapBundle\Tree\RecursiveIterator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TreeExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('tree', [$this, 'createRecursiveIterator']),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('recursive', [$this, 'convertToRecursiveIterator']),
        ];
    }

    public function createRecursiveIterator($iterator, $mode = \RecursiveIteratorIterator::SELF_FIRST, $flags = 0)
    {
        return new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::SELF_FIRST, $flags);
    }

    public function convertToRecursiveIterator($iterator)
    {
        return new RecursiveIterator($iterator);
    }
}
