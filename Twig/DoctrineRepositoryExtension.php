<?php

namespace CP\CreapBundle\Twig;

use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DoctrineRepositoryExtension extends AbstractExtension
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getFunctions()
    {
        $em = $this->doctrine->getManager();
        $factory = $em->getMetadataFactory();
        $functions = [];

        /* @var $metadata ClassMetadata */
        foreach ($factory->getAllMetadata() as $metadata) {
            $reflection = $metadata->getReflectionClass();
            if ($reflection->isAbstract()) {
                continue;
            }

            $name = strtolower(preg_replace('/([a-z])([A-Z])/', '\\1_\\2', Inflector::pluralize($reflection->getShortName())));
            $functions[] = new TwigFunction($name, function() use ($em, $reflection) {
                return $em->getRepository($reflection->name);
            });
        }

        return $functions;
    }
}
