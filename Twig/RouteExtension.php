<?php
/**
 * Created by PhpStorm.
 * User: behe
 * Date: 2.10.17
 * Time: 20:31
 */

namespace CP\CreapBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RouteExtension extends AbstractExtension
{
    protected $container;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('routeExists', array($this, 'routeExists')),
        );
    }

    public function routeExists($name)
    {
        $router = $this->container->get('router');
        return (null === $router->getRouteCollection()->get($name)) ? false : true;
    }
}